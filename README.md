# Example project to demonstrate custom rule-sets


This is an example project to demonstrate how you can create a custom ruleset
on top of the default rule-set that is shipped with the default Git semgrep
analyzer.

This project follows the approach that is detailed in https://about.gitlab.com/blog/2021/12/21/rule-pack-synthesis/.

1. Adding a custom semgrep rule to the repository [myrule.yml](./myrule.yml)
   which includes secret detection patterns for Java that look for substrings
   in variable names that may be used to store secrets. The semgrep pattern below
   can be easily adjusted by adding more substrings to the regular expression pattern. 
   For example if you want to cover all variables that carry `important` in their name,
   the pattern can be adjusted to: `"(?i)[^\\[]*(password|pass|passwd|pwd|secret|token|secrete|important)[^\\]]*"`.
   Note that the pattern ignores case. 
```yml
- id: "java.exposure"
  patterns:
  - pattern-either:
    - pattern: "$TYPE [] $MASK;"
    - pattern: "$TYPE [] $MASK = ...;"
    - pattern: "$TYPE $MASK;"
    - pattern: "$TYPE $MASK = ...;"
  - metavariable-regex:
      metavariable: "$MASK"
      regex: "(?i)[^\\[]*(password|pass|passwd|pwd|secret|token|secrete)[^\\]]*"
  message: "My personal rule -- Hardcoded secret detected"
  metadata:
    cwe: "CWE-200: Exposure of Sensitive Information to an Unauthorized Actor"
  severity: "WARNING"
  languages:
  - "java"
```
2. Adding a custom ruleset configuration
   [sast-ruleset.toml](.gitlab/sast-ruleset.toml) that adds the custom rules to
   the standard ruleset (`/rules/find_sec_bugs.yml`) that is already shipped with the 
   GitLab analyzer.

As depicted in the figure below, the findings yielded by `myrule.yml` will show up in the Vulnerability report.

![2022-08-24-20-46-40.gif](./2022-08-24-20-46-40.gif)
